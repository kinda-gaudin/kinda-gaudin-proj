<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Http\Controllers\PersonController;
use App\Http\Controllers\RedirectController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\AdhererController;
use App\Models\User;
use App\Models\Event;
use App\Models\Contact;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * URL d'entrée du site de l'association
 */
Route::get('/', function (Request $request) {
    $events = DB::table('events')
        ->orderBy('begindate', 'desc')
        ->get();

    // La variable events dans accueil.blade.php prend la valeur 
    // de la collection $events.

    $user = $request->user();
    return view('accueil', ['events' => $events, 'user' => $user]);
});

/**
 * T E S T S
 */
Route::get('/asso', function (Request $request) {
    $events = DB::table('events')
        ->orderBy('begindate', 'desc')
        ->get();

    // Retrieve the currently authenticated user...
    $user = Auth::user();

    // Retrieve the currently authenticated user's ID...
    $id = Auth::id();

    // La variable events dans accueil.blade.php prend la valeur 
    // de la collection $events.

    $user = $request->user();
    return view('accueil', ['events' => $events, 'user' => $user]);
});

/**
 * Formulaire pour enregistrer un utilisateur
 */
Route::get('/register', function () {
    return view('auth.register');
});

/**
 * Contrôle du formulaire d'enregistrement d'un compte
 * Cryptage du mot de passe
 * Enregistrement dans la table users
 * A noter : la table users est nécessaire à l'authentification 
 */
Route::post('/register', function (Request $request) {
    $data = $request->validate([
        'name' => 'required|string|max:255',
        'firstname' => 'required|string|max:255',
        'email' => 'required|string|email|max:255|unique:users',
        'password' => ['required', 'confirmed'],
    ]);

    // $link = tap(new App\Models\Person($data))->save();

    User::create([
        'name' => $request->name,
        'firstname' => $request->firstname,
        'dateofbirthday' => $request->dateofbirthday,
        'email' => $request->email,
        //Le mot de passe est crypté dans la base
        'password' => Hash::make($request->password),
        // Par défaut le rôle est "visiteur"
        //'role' => $request->role,
    ]);

    return redirect('/');
});

/**
 * Contrôle du formulaire d'enregistrement d'un contact
 * Enregistrement dans la table contacts
 */
Route::post('/contact', function (Request $request) {
    $data = $request->validate([
        'name' => 'required|string|max:255',
        'email' => 'required|string|email|max:255',
        'subject' => 'required|string|max:255',
        'message' => 'required|string|max:255',
    ]);

    Contact::create([
        'name' => $request->name,
        'email' => $request->email,
        'subject' => $request->subject,
        'message' => $request->message,
    ]);

    return redirect('/');
});

/**
 * Formulaire de connexion
 */
Route::get('/login', function () {
    return view('auth.login');
});

/** *******************************************************************************
 * JAVASCRIPT PREVISIONS METEO A 5 JOURS
 * *******************************************************************************
 */
Route::get('/meteo', function () {
    return view('meteo');
});

/**
 * Authentification de l'utilisateur
 */
Route::post('/login', [LoginController::class, 'authenticate']);

/**
 * Déconnexion de l'utilisateur
 */
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

/**
 * Page d'administration
 */
Route::get('/admi', function (Request $request) {
    if (Auth::check()) {
        $events = \App\Models\Event::all();
        // La variable events dans accueil.blade.php prend la valeur 
        // de la collection $events.
        $user = $request->user();
        $contacts = \App\Models\Contact::all();
        return view('administration', ['events' => $events, 'user' => $user, 'contacts' => $contacts]);
    }
});

/**
 * T E S T S
 */
Route::get('/testeve', '\App\Http\Controllers\EventController@testEvent');
Route::get('/show_event/{id}', '\App\Http\Controllers\EventController@show');

/**
 * Suppression d'un évènement
 */
Route::get('/delete_event/{id}', '\App\Http\Controllers\EventController@delete');

/**
 * Formulaire pour ajouter un évènement
 */
Route::get('/add_event', function (Request $request) {
    $user = $request->user();
    return view('add_event', ['user' => $user]);
});

Route::post('/add_event', '\App\Http\Controllers\EventController@addEvent');

/**
 * Formulaire pour modifier un évènement
 */
Route::get('/edit_event/{id}', function ($id, Request $request) {
    $ev = Event::find($id);
    $user = $request->user();
    return view('edit_event', ['event' => $ev, 'user' => $user]);
});

Route::post('/edit_event', '\App\Http\Controllers\EventController@editEvent');

/**
 * Rechercher un évènement
 */
Route::post('/search_event', '\App\Http\Controllers\EventController@searchEvent');

/**
 * Formulaire pour modifier le compte user
 */
Route::get('/edit_user', function (Request $request) {
    $user = $request->user();
    return view('auth.edit_user', ['user' => $user]);
});

Route::post('/edit_user', function (Request $request) {
    $data = $request->validate([
        'name' => 'required|string|max:255',
        'firstname' => 'required|string|max:255',
        'email' => 'required|string|email|max:255|unique:users',
        'password' => ['required', 'confirmed'],
    ]);

    $user = $request->user();

    // $link = tap(new App\Models\User($data))->save();

    $user->name = $request->name;
    $user->firstname = $request->firstname;
    $user->dateofbirthday = $request->dateofbirthday;
    $user->email = $request->email;
    //Le mot de passe est crypté dans la base
    $user->password = Hash::make($request->password);
    // Par défaut le rôle est "visiteur"
    //'role' => $request->role,
    $user->save();

    return redirect('/');
});

////////////////////////////////////////////// T E S T S //////////////////////////////////////////////

Route::get('/event1', '\App\Http\Controllers\EventController@createEvent1');
Route::get('/event2', '\App\Http\Controllers\EventController@createEvent2');

