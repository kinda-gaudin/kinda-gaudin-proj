CREATE DATABASE  IF NOT EXISTS `kinda_gaudin_db` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `kinda_gaudin_db`;
-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: kinda_gaudin_db
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `addresss`
--

DROP TABLE IF EXISTS `addresss`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `addresss` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `number` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `town` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `district` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone1` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone2` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `enterprise_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addresss`
--

LOCK TABLES `addresss` WRITE;
/*!40000 ALTER TABLE `addresss` DISABLE KEYS */;
INSERT INTO `addresss` VALUES (1,'1','rue','des plantes','Paris','Paris','75011','France','01','02','http://localhost:8000/asso',1,1,NULL,NULL,NULL),(2,'20','rue','des Plantes','Paris','Paris','75015','France',NULL,NULL,NULL,NULL,28,NULL,'2021-08-14 11:36:08','2021-08-14 11:36:08');
/*!40000 ALTER TABLE `addresss` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtitle` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(10000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` datetime DEFAULT NULL,
  `workinggroup_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles`
--

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` VALUES (1,'Les espèces en voie de disparition en France','La France n\'est pas épargnée par la disparition d\'espèces','bla bla','2021-03-04 00:00:00',1,'2021-03-04 13:50:27','2021-03-04 13:51:29'),(2,'Les énergies durables','Comment remplacer les énergies fossiles','bla bla','2021-03-04 00:00:00',1,'2021-03-04 13:50:43','2021-03-04 13:51:29');
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `contacts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` VALUES (1,'Paul Dupond','paul@paul.fr','pollution','pollution des ruisseaux de forêt','2021-05-29 18:15:46','2021-05-29 18:15:46'),(2,'Martin Durand','dur@dur.fr','plantation d\'arbres','plantation d\'arbres','2021-05-29 18:44:17','2021-05-29 18:44:17'),(3,'Dupont Pierre','pierre@a.fr','Randonnées en forêt','Organiser des randonnées en forêt','2021-05-30 07:52:31','2021-05-30 07:52:31');
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contributions`
--

DROP TABLE IF EXISTS `contributions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `contributions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `year` int(11) NOT NULL,
  `amount` double(8,2) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `contributions_year_unique` (`year`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contributions`
--

LOCK TABLES `contributions` WRITE;
/*!40000 ALTER TABLE `contributions` DISABLE KEYS */;
/*!40000 ALTER TABLE `contributions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enterprises`
--

DROP TABLE IF EXISTS `enterprises`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `enterprises` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `siren` varchar(9) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `siret` varchar(14) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `immatriculation` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `partner` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enterprises`
--

LOCK TABLES `enterprises` WRITE;
/*!40000 ALTER TABLE `enterprises` DISABLE KEYS */;
INSERT INTO `enterprises` VALUES (1,'BNP PAribas',NULL,NULL,NULL,NULL,NULL,'2021-01-28 13:46:44','2021-01-28 13:46:44'),(2,'Soc Générale',NULL,NULL,NULL,NULL,NULL,'2021-01-28 14:23:28','2021-01-28 14:23:28'),(3,'Soc Générale',NULL,NULL,NULL,NULL,NULL,'2021-01-28 14:30:39','2021-01-28 14:30:39'),(4,'LCL',NULL,NULL,NULL,NULL,NULL,'2021-01-28 14:32:26','2021-01-28 14:32:26');
/*!40000 ALTER TABLE `enterprises` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `begindate` datetime DEFAULT NULL,
  `enddate` datetime DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (4,'Nettoyage de la forêt','2021-05-01 00:00:00','2021-05-15 00:00:00','Nettoyage de la forêt. Chacun prend une poubelle.','2021-01-28 17:01:40','2021-05-24 13:20:31'),(6,'Découverte de la flore de la  forêt','2021-06-01 00:00:00','2021-06-15 00:00:00','Explications de l\'éco système de la forêt. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi.','2021-01-28 17:01:40','2021-01-28 17:01:40'),(8,'Camping en forêt','2021-08-07 00:00:00','2021-08-08 00:00:00','Camping et dormir à la belle étoile dans la forêt de Montmorency.','2021-01-28 17:01:40','2021-05-30 07:55:03'),(9,'Découverte de la flore de la  forêt','2021-07-01 00:00:00','2021-07-15 00:00:00','Explications de l\'éco système de la forêt. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi.','2021-01-28 17:01:40','2021-01-28 17:01:40'),(21,'Planter des arbres et des cerisiers.','2021-05-25 00:00:00','2021-05-25 00:00:00','Planter des arbres. Allez hop !','2021-05-24 13:24:54','2021-05-24 17:08:21'),(22,'Créer des lieux de \"street workaround\"','2021-08-01 00:00:00','2021-08-01 00:00:00','Créer des lieux de \"street workaround\"','2021-05-24 17:09:34','2021-05-24 17:09:34'),(23,'Créer une ferme','2021-09-01 00:00:00','2022-12-31 00:00:00','Créer une ferme pour éduquer les enfants','2021-05-24 18:27:18','2021-05-24 18:27:18'),(24,'Entretenir les chemins en forêt','2021-07-05 00:00:00','2021-07-30 00:00:00','Entretenir les chemins en forêt','2021-05-29 06:42:40','2021-05-30 07:59:03'),(28,'Découverte de la flore de la  forêt',NULL,NULL,'Explications de l\'éco système de la forêt','2021-08-14 11:36:08','2021-08-14 11:36:08');
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `informations`
--

DROP TABLE IF EXISTS `informations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `informations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `presidentword` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `informations`
--

LOCK TABLES `informations` WRITE;
/*!40000 ALTER TABLE `informations` DISABLE KEYS */;
INSERT INTO `informations` VALUES (1,'Association de Défense de l\'Environnement et du Cadre de vie Bouffémont','Association de défense de l\'environnement qui lutte pour préserver le Cadre de vie sur Bouffémont','asso-env-bouffemont@asso.fr','2021-03-04 15:27:19','2021-03-04 15:27:19');
/*!40000 ALTER TABLE `informations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1),(4,'2021_01_09_200151_create_persons_table',1),(5,'2021_01_19_155635_create_contributions_table',2),(6,'2021_01_19_164512_create_contributions_table',3),(7,'2021_01_19_164654_create_contributions_table',4),(8,'2021_01_19_183424_create_contributions_table',5),(9,'2021_01_25_090532_create_events_table',6),(10,'2021_01_25_100235_create_addresss_table',7),(11,'2021_01_26_174500_create_persons_table',8),(12,'2021_01_26_191000_create_persons_table',9),(13,'2021_01_26_191500_create_persons_table',10),(14,'2021_01_27_101903_create_enterprises_table',11),(15,'2021_01_28_151500_create_addresss_table',12),(16,'2021_01_28_151500_create_enterprises_table',12),(17,'2021_01_28_151600_create_addresss_table',13),(18,'2021_01_28_151600_create_enterprises_table',13),(19,'2021_01_28_153000_create_addresss_table',14),(20,'2021_01_28_173000_create_events_table',15),(21,'2021_01_28_174500_create_persons_table',16),(22,'2021_01_28_174500_create_addresss_table',17),(23,'2021_01_28_183500_create_addresss_table',18),(24,'2021_02_02_114500_create_persons_table',19),(25,'2021_02_02_161207_create_workinggroups_table',19),(26,'2021_03_03_141850_create_projects_table',20),(27,'2021_03_04_090711_create_articles_table',21),(28,'2021_03_04_153737_create_informations_table',22),(29,'2021_03_04_182208_create_newsletters_table',23),(30,'2021_05_21_120000_create_persons_table',24),(31,'2021_05_21_130000_create_failed_jobs_table',25),(32,'2021_05_21_130000_create_password_resets_table',25),(33,'2021_05_21_230000_create_users_table',26),(34,'2021_05_22_120000_create_addresss_table',27),(35,'2021_05_22_120000_create_contributions_table',27),(36,'2021_05_22_120000_create_users_table',27),(37,'2021_05_29_172904_create_contacts_table',28);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletters`
--

DROP TABLE IF EXISTS `newsletters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `newsletters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` datetime DEFAULT NULL,
  `content` varchar(10000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletters`
--

LOCK TABLES `newsletters` WRITE;
/*!40000 ALTER TABLE `newsletters` DISABLE KEYS */;
INSERT INTO `newsletters` VALUES (1,'Tombola au profit du nettoyage de la ville','2021-03-04 00:00:00','Tombola au profit du nettoyage de la ville','2021-03-04 17:43:41','2021-03-04 17:43:41'),(2,'Tombola au profit du nettoyage de la ville','2021-03-04 00:00:00','Tombola au profit du nettoyage de la ville','2021-03-04 17:45:25','2021-03-04 17:45:25'),(3,'Tombola au profit du nettoyage de la ville','2021-03-04 00:00:00','Tombola au profit du nettoyage de la ville','2021-03-04 17:45:32','2021-03-04 17:45:32');
/*!40000 ALTER TABLE `newsletters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `projects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `goal` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `begindate` datetime DEFAULT NULL,
  `enddate` datetime DEFAULT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `workinggroup_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects`
--

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
INSERT INTO `projects` VALUES (1,'Plantations d\'arbres en forêt de Montmorency','Régénérer la forêt','2021-05-01 00:00:00','2022-05-01 00:00:00','',1,'2021-03-03 14:14:22','2021-03-03 14:17:39'),(2,'Construction d\'une héolienne','Sensibiliser les adolescents','2021-05-01 00:00:00','2022-05-01 00:00:00','',1,'2021-03-03 14:15:22','2021-03-03 14:17:39');
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateofbirthday` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'visiteur',
  `workinggroup_id` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (10,'Gaudin','Kinda','1976-10-22','kinda2@kinda.fr',NULL,'$2y$10$tV1YBsH8rZ3DWAZ7g5zYQuOP551aM7uSrvypsAI4Q3fM6HN/0O51q','visiteur',NULL,NULL,'2021-05-30 07:45:27','2021-05-30 07:47:28'),(11,'kinda','Gaudin','1976-10-21','kindagaudin@kinda.fr',NULL,'$2y$10$4dbKKl0.byd7nYFSOp5/S.tArToeKdtVTv8CzTGef5KPmjhIM9dy6','visiteur',NULL,NULL,'2021-05-30 07:50:35','2021-05-30 07:50:35'),(12,'a','a','2001-01-01','a@a.a',NULL,'$2y$10$WmvNY4HM.8Py66bu6VPDa.lSpFCeaxqJl5ZVDq0toboqtL32G30.m','visiteur',NULL,NULL,'2021-07-27 11:32:44','2021-07-27 11:32:44'),(13,'rayan','rayan','2009-09-23','rayan@rayan.fr',NULL,'$2y$10$rEnWXWVFczF00FR8Tksgs.ERDQ8nZF1o9LByvw0/64kl0kA8Enham','visiteur',NULL,NULL,'2021-08-04 11:40:15','2021-08-04 11:40:15'),(14,'kinda','kinda','2001-01-01','kinda@kinda.fr',NULL,'$2y$10$hoWdHRZEIHPDIkP5n817Mu07ox9ynNY7EbY3239mQK.PjCD9lkSH2','visiteur',NULL,NULL,'2021-08-14 07:48:02','2021-08-14 07:48:02');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workinggroups`
--

DROP TABLE IF EXISTS `workinggroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `workinggroups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workinggroups`
--

LOCK TABLES `workinggroups` WRITE;
/*!40000 ALTER TABLE `workinggroups` DISABLE KEYS */;
INSERT INTO `workinggroups` VALUES (1,'Groupe l\'environnement expliqué aus enfants','2021-03-03 10:06:14','2021-03-03 10:06:14');
/*!40000 ALTER TABLE `workinggroups` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-14 23:17:30
