<?php

namespace Database\Factories;

use App\Models\Workinggroug;
use Illuminate\Database\Eloquent\Factories\Factory;

class WorkinggrougFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Workinggroug::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
