<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddresssTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresss', function (Blueprint $table) {
            $table->increments('id');
            $table->string('number', 10)->nullable();
            $table->string('type', 20)->nullable();
            $table->string('name', 50)->nullable();
            $table->string('town', 50)->nullable();
            $table->string('district', 50)->nullable();
            $table->string('code', 5)->nullable();
            $table->string('country', 50)->nullable();
            $table->string('phone1', 20)->nullable();
            $table->string('phone2', 20)->nullable();
            $table->string('site', 30)->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('event_id')->nullable(); 
            $table->integer('enterprise_id')->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresss');
    }
}
