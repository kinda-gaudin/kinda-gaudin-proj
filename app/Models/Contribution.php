<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contribution extends Model
{
    use HasFactory;
    protected $table = 'contributions';
    protected $primaryKey = 'id';
    protected $year = 'year';
    protected $amount = 'amount';
    protected $user_id = 'user_id';

    protected $fillable = [
        'year',
        'amount',
        'user_id'
    ];

    /**
     * Contribution * -- 1 Person
     * C'est la relation inverse créé dans Person
     * Person 1 -- * Contribution
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

/*     public function person()
    {
        return $this->hasOne(Person::class);
    } */
}
