<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * Cette classe représente les informations propres à un utilisateur
 * (visiteur, adherent, secretaire, tresorier, president).
 * Elle est aussi utilisée par Laravel pour l'authentification
 * et le cryptage des mots de passe en base.
 */
class User extends Authenticatable
{
    use HasFactory, Notifiable;


   
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'firstname',
        'dateofbirthday',
        'email',
        'password',
        'role',
        'workinggroup_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

/**
     * Relation 1 à plusieurs
     * Person 1 -- * Contribution
     */
    public function contributions()
    {
        return $this->hasMany(Contribution::class);
    }

    /**
     * Relation 1 -- 1
     * Person 1 -- 1 Address
     */
    public function address()
    {
        return $this->hasOne(Address::class);
    }

    /**
     * Person * -- 1 Workinggroup
     * C'est la relation inverse créée dans Workinggroup
     * Workinggroup 1 -- * Person
     */
    public function workinggroup()
    {
        return $this->belongsTo(Workinggroup::class);
    }
}
