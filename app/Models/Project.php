<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;
    protected $table = 'projects';
    protected $primaryKey = 'id';
    protected $label = 'label';
    protected $goal = 'goal';
    protected $begindate = 'begindate';
    protected $enddate = 'enddate';
    protected $image = 'image';
    protected $workinggroup_id = 'workinggroup_id';

    protected $fillable = [
        'label',
        'goal',
        'begindate',
        'enddate',
        'image',
        'workinggroup_id'
    ];

    /**
     * Project * -- 1 Workinggroup
     * C'est la relation inverse créée dans Workinggroup
     * Workinggroup 1 -- * Project
     */
    public function workinggroup()
    {
        return $this->belongsTo(Workinggroup::class);
    }
}
