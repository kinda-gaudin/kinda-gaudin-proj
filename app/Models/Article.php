<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;
    protected $table = 'articles';
    protected $primaryKey = 'id';
    protected $title = 'title';
    protected $subtitle = 'subtitle';
    protected $content = 'content';
    protected $date = 'date';
    protected $workinggroup_id = 'workinggroup_id';

    protected $fillable = [
        'title',
        'subtitle',
        'content',
        'date',
        'workinggroup_id'
    ];

    /**
     * Article * -- 1 Workinggroup
     * C'est la relation inverse créée dans Workinggroup
     * Workinggroup 1 -- * Article
     */
    public function workinggroup()
    {
        return $this->belongsTo(Workinggroup::class);
    }
}
