<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory;
    protected $table = 'addresss';
    protected $primaryKey = 'id';
    protected $number = 'number';
    protected $type = 'type';
    protected $name = 'name';
    protected $town = 'town';
    protected $district = 'district';
    protected $code = 'code';
    protected $country = 'country';
    protected $phone1 = 'phone1';
    protected $phone2 = 'phone2';
    protected $site = 'site';
    protected $user_id = 'user_id';
    protected $event_id = 'event_id';
    protected $enterprise_id = 'enterprise_id';


    protected $fillable = [
        'number',
        'type',
        'name',
        'town',
        'district',
        'code',
        'country',
        'phone1',
        'phone2',
        'site',
        'user_id',
        'event_id',
        'enterprise_id'
    ];

    /**
     * Relation inverse 1 -- 1
     * Address  1 -- 1 Event
     */
    /*     public function event()
    {
        return $this->belongsTo(Event::class);
    } */

    /**
     * Relation inverse 1 -- 1
     * Address  1 -- 1 Enterprise
     * Il faut ajouter la propriété address_id dans la classe Enterprise
     */
/*     public function enterprise()
    {
        return $this->belongsTo(Enterprise::class);
    } */

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
