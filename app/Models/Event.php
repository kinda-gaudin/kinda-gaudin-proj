<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;
    protected $table = 'events';
    protected $primaryKey = 'id';
    protected $label = 'label';
    protected $begindate = 'begindate';
    protected $enddate = 'enddate';
    protected $content = 'content';

    protected $fillable = [
        'label',
        'begindate',
        'enddate',
        'content',
    ];

    /**
     * Relation 1 -- 1
     * Event 1 -- 1 Address
     */
    public function address()
    {
        return $this->hasOne(Address::class);
    }
}
