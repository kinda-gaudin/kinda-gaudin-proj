<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Information extends Model
{
    use HasFactory;
    protected $table = 'informations';
    protected $primaryKey = 'id';
    protected $description = 'description';
    protected $presidentword = 'presidentword';
    protected $contact = 'contact';

    protected $fillable = [
        'description',
        'presidentword',
        'contact'
    ];
}
