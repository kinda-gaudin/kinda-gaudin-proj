<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Workinggroup extends Model
{
    use HasFactory;
    protected $table = 'workinggroups';
    protected $primaryKey = 'id';
    protected $name = 'name';

    protected $fillable = [
        'name'
    ];

    /**
     * Relation 1 à plusieurs
     * Workinggroup 1 -- * Person
     */
    public function persons()
    {
        return $this->hasMany(Person::class);
    }

    /**
     * Relation 1 à plusieurs
     * Workinggroup 1 -- * Project
     */
    public function projects()
    {
        return $this->hasMany(Project::class);
    }

    /**
     * Relation 1 à plusieurs
     * Workinggroup 1 -- * Article
     */
    public function articles()
    {
        return $this->hasMany(Article::class);
    }
}
