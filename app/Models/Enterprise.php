<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Enterprise extends Model
{
    use HasFactory;
    protected $table = 'enterprises';
    protected $primaryKey = 'id';
    protected $name = 'name';
    protected $type = 'type';
    protected $siren = 'siren';
    protected $siret = 'siret';
    protected $immatriculation = 'immatriculation';
    protected $partner = 'partner';

    protected $fillable = [
        'name',
        'type',
        'siren',
        'siret',
        'immatriculation',
        'partner'
    ];

    /**
     * Relation 1 -- 1
     * Enterprise 1 -- 1 Address
     */
    public function address()
    {
        return $this->hasOne(Address::class);
    }
}
