<?php

namespace App\Http\Controllers;

use App\Models\Address;
use Illuminate\Http\Request;
use App\Models\Enterprise;

class EnterpriseController extends Controller
{
    public function show($id)
    {
        $ent = Enterprise::find($id);
        echo $ent->name;
        echo ' | ';
        echo $ent->address->number;
        echo ' | ';
        echo  $ent->address->type;
        echo ' | ';
    }

    public function delete($id)
    {
        $ent = Enterprise::find($id);
/*         echo $ent->name;
        echo ' : supprimé de la base'; */
        $ent->delete($id);
    }

    public function testEnterprise() 
    {
        $addr = new Address(['number' => 17, 'type' => 'rue', 'name' => "des Plantes", 'town' => 'Paris', 'district' => 'Paris', 'code' => '75015', 'country' => 'France']);
        $addr->save();
        $ent = new Enterprise(['name' => 'LCL']) ;
        $ent -> save();
        // hasOne
        $ent->address()->save($addr);
        // belongsTo : non impléméentée
        // $addr->enterprise()->$ent;
    }
}
