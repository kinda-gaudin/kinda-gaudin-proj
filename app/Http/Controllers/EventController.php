<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Contact;

class EventController extends Controller
{
    /**
     * T E S T S
     * création adress 1
     */
    public function createAddress1()
    {
        Address::create([
            'number' => '15',
            'type' => 'rue',
            'name' => 'des Plantes',
            'town' => 'Paris',
            'district' => 'Paris',
            'code' => '75015',
            'country' => 'France',
            'mail' => 'toto@toto.fr',
            'phone1' => '+33 100000000',
            'phone2' => '+33 200000000',
            'site' => 'https://www.urbanisation-si.com/'
        ]);
    }

    /**
     * T E S T S
     * création event 1
     */
    public function createEvent1()
    {
        EventController::class::createAddress1();

        $address = Address::all()->last();

        Event::create([
            'label' => 'Rammassge de déchets en forêt',
            'begindate' => '2021-01-24 10:00:00',
            'enddate' => '2021-01-24 16:00:00',
            'content' => 'Rammassge de déchets en forêt de Montmorency. Rendez-vous à l\'étang Marie',
            'address_id' => $address->id
        ]);
    }

    /**
     * T E S T S
     * création event 2
     */
    public function createEvent2()
    {
        EventController::class::createAddress1();

        $address = Address::all()->last();

        Event::create([
            'label' => 'Sortie avec le collège',
            'begindate' => '2021-01-27 09:15:00',
            'enddate' => '2021-01-27 17:30:00',
            'content' => 'Sortie avec le collège. Visite d\'une usine de recyclage',
            'address_id' => $address->id
        ]);
    }

    /**
     *      ************************************************************
            Les requêtes Laravel Eloquent sont mises en commentaires et sont remplacées par du SQL natif
            **********************************************************
     * Crée un évènement, appelé par le controlleur.
     */
    public function addEvent(Request $request)
    {
        $data = $request->validate([
            'label' => 'required|string|max:255',
            'content' => 'required|string|max:255'
        ]);

        // $link = tap(new App\Models\Person($data))->save();

/*         Event::create([
            'label' => $request->label,
            'begindate' => $request->begindate,
            'enddate' => $request->enddate,
            'content' => $request->content
        ]);
 */
        DB::insert('insert into events (label, begindate, enddate, content) values (?, ?, ?, ?)', [$request->label, $request->begindate, $request->enddate, $request->content]);

        /*         $events = Event::all();
        return view('administration', ['events' => $events]); */

        return redirect('/admi');

        /*        EventController::class::createAddress1();

        $address = Address::all()->last();

        Event::create([
            'label' => 'Rammassge de déchets en forêt',
            'begindate' => '2021-01-24 10:00:00',
            'enddate' => '2021-01-24 16:00:00',
            'content' => 'Rammassge de déchets en forêt de Montmorency. Rendez-vous à l\'étang Marie',
            'address_id' => $address->id
        ]); */
    }
    /**
     *      *************************************************************
            Les requêtes Laravel Eloquent sont mises en commentaires et sont remplacées par du SQL natif
            **********************************************************
     * Modifie un évènement
     * Appelé par le controlleur
     */
    public function editEvent(Request $request)
    {
        $data = $request->validate([
            'label' => 'required|string|max:255',
            'content' => 'required|string|max:255'
        ]);

        $id = $request->id;
        $event = Event::find($id);

        $user = $request->user();

        // $link = tap(new App\Models\Person($data))->save();


/*         $event->label = $request->label;
        $event->begindate = $request->begindate;
        $event->enddate = $request->enddate;
        $event->content = $request->content;
        $event->save(); */

        DB::update(
            'update events set label = ?, begindate = ?, enddate = ?, content = ? where id = ?',
                [$request->label, $request->begindate, $request->enddate, $request->content, $request->id ]
        );

        $events = Event::all();
        return view('administration', ['events' => $events, 'user' => $user]);

        //return redirect('/admi');

        /*        EventController::class::createAddress1();

        $address = Address::all()->last();

        Event::create([
            'label' => 'Rammassge de déchets en forêt',
            'begindate' => '2021-01-24 10:00:00',
            'enddate' => '2021-01-24 16:00:00',
            'content' => 'Rammassge de déchets en forêt de Montmorency. Rendez-vous à l\'étang Marie',
            'address_id' => $address->id
        ]); */
    }

    /**
     *      **********************************************************
            Les requêtes Laravel Eloquent sont mises en commentaires et sont remplacées par du SQL natif
            Ajout d'une jointure gauche pour avoir l'adresse de l'évènement si elle existe.
            **********************************************************
     * Rechercher un évènement
     * Tous les champs à vide => tous les évènements
     * Uniquement "Titre" => tous les évènements comportant la chaîne de caractère dans le titre
     * Uniquement "Période début" ET "Période fin" => tous les évènements dont la date de début est comprise dans la période
     * Uniquement "Description" => tous les évènements comportant la chaîne de caractère dans la description
     * Uniquement "Titre" ET "Période début" ET "Période fin" => tous les évènements comportant la chaîne de caractère dans le titre et dont la date de début est comprise dans la période
     */
    public function searchEvent(Request $request)
    {
        if (empty($request->label) && empty($request->begindate) && empty($request->enddate) && empty($request->content)) {
/*             $searchEvents = DB::table('events')
                ->orderBy('begindate', 'desc')
                ->get(); */
                $searchEvents = DB::select('select e.*, a.* from events as e left join addresss as a on e.id=a.event_id order by begindate desc');
        } else if (!empty($request->label) && empty($request->begindate) && empty($request->enddate) && empty($request->content)) {
/*             $searchEvents =  DB::table('events')
                ->where('label', 'like', "%" . $request->label . "%")
                ->orderBy('begindate', 'desc')
                ->get(); */
                // $results = DB::select('select * from users where id = :id', ['id' => 1]);
                $searchEvents = DB::select('select e.*, a.* from events as e left join addresss as a on e.id=a.event_id where label like :filtre order by begindate desc', 
                    ['filtre' => "%" . $request->label . "%"]);
        } else if (empty($request->label) && !empty($request->begindate) && !empty($request->enddate) && empty($request->content)) {
/*             $searchEvents =  DB::table('events')
                ->whereBetween('begindate', [$request->begindate, $request->enddate])
                ->orderBy('begindate', 'desc')
                ->get(); */
                $searchEvents = DB::select('select e.*, a.* from events as e left join addresss as a on e.id=a.event_id where begindate between :begindate and :enddate order by begindate desc', 
                    [':begindate' => $request->begindate, ':enddate' => $request->enddate]);
        } else if (empty($request->label) && empty($request->begindate) && empty($request->enddate) && !empty($request->content)) {
/*               $searchEvents =  DB::table('events')
                 ->where('content', 'like', "%" . $request->content . "%")
                ->orderBy('begindate', 'desc')
                ->get(); */
                $searchEvents = DB::select('select e.*, a.* from events as e left join addresss as a on e.id=a.event_id where content like :filtre order by begindate desc', 
                    ['filtre' => "%" . $request->content . "%"]);
        } else {
            // $searchEvents =  DB::table('events')
                // ->where('label', 'like', "%" . $request->label . "%")
                /*              ->orWhere('begindate', '=', $request->begindate) */
                // ->whereBetween('begindate', [$request->begindate, $request->enddate])
                // ->orderBy('begindate', 'desc')
                // ->get();
                $searchEvents = DB::select('select e.*, a.* from events as e left join addresss as a on e.id=a.event_id where label like :filtre and begindate between :begindate and :enddate order by begindate desc', 
                    ['filtre' => "%" . $request->label . "%", ':begindate' => $request->begindate, ':enddate' => $request->enddate]);
        }
        $events = Event::all();
        $user = $request->user();
        $contacts = \App\Models\Contact::all();
        return view('administration', ['events' => $events, 'searchEvents' => $searchEvents, 'user' => $user, 'contacts' => $contacts]);
    }

    /**
     * T E S T S
     */
    public function show($id)
    {
        $ent = Event::find($id);
        echo $ent->label;
        echo ' | ';
        echo $ent->address->number;
        echo ' | ';
        echo  $ent->address->type;
        echo ' | ';
    }

    /**
     *      *************************************************************
            Les requêtes Laravel Eloquent sont mises en commentaires et sont remplacées par du SQL natif
            **********************************************************
     * Supprine un évènement
     * Appelé par le controller
     */
    public function delete($id)
    {
        $ent = Event::find($id);
        /*         echo $ent->name;
        echo ' : supprimé de la base'; */

        // $ent->delete($id);
        DB::delete('delete from events where id = ?', [$id]);

        /*         $events = Event::all();
        return view('administration', ['events' => $events]); */
        return redirect('/admi');
    }

    /**
     * T E S T S
     */
    public function testEvent()
    {
        $addr = new Address(['number' => 20, 'type' => 'rue', 'name' => "des Plantes", 'town' => 'Paris', 'district' => 'Paris', 'code' => '75015', 'country' => 'France']);
        $addr->save();
        $eve = new Event(['label' => 'Découverte de la flore de la  forêt', 'content' => 'Explications de l\'éco système de la forêt']);
        $eve->save();
        // hasOne
        $eve->address()->save($addr);
    }
}
