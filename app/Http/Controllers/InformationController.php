<?php

namespace App\Http\Controllers;
use App\Models\Information;

use Illuminate\Http\Request;

class InformationController extends Controller
{
        /**
     * Test
     */
    public function saveInformation()
    {
        $inf = new Information([
            'description' => 'Association de Défense de l\'Environnement et du Cadre de vie Bouffémont',
            'presidentword' => 'Association de défense de l\'environnement qui lutte pour préserver le Cadre de vie sur Bouffémont',
            'contact' => 'asso-env-bouffemont@asso.fr'
        ]);
        $inf->save();
    }
}
