<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Newsletter;

class NewsletterController extends Controller
{
        /**
     * Test
     */
    public function saveNewsletter()
    {
        $nl = new Newsletter([
            'title' => 'Tombola au profit du nettoyage de la ville',
            'date' => '2021-03-04',
            'content' => 'Tombola au profit du nettoyage de la ville'
        ]);
        $nl->save();
    }
}
