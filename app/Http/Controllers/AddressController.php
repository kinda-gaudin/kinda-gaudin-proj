<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Address;

class AddressController extends Controller
{
    public function show($id)
    {
        $add = Address::find($id);
        echo $add->number;
        echo ' | ';
        echo $add->type;
        echo ' | ';
        echo  $add->name;
        echo ' | ';
        echo $add->town;
        echo ' | ';
        echo $add->district;
        echo ' | ';
        echo  $add->code;
        echo ' | ';
    }

    public function delete($id)
    {
        $add = Address::find($id);
/*         echo $ent->name;
        echo ' : supprimé de la base'; */
        $add->delete($id);
    }

    public function testAddress() 
    {
        $add = new Address(['number' => 1, 'type' => 'rue', 'name' => "des Arbres", 'town' => 'Paris', 'district' => 'Paris', 'code' => '75015', 'country' => 'France']);
        $add->save();
    }
}
