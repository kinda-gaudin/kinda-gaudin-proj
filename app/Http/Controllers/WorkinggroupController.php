<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Workinggroup;
use App\Models\Person;
use App\Models\Project;
use App\Models\Article;

class WorkinggroupController extends Controller
{
    /**
     * Test
     */
    public function show($id)
    {
        $wg = Workinggroup::find($id);
        echo $wg->name;
    }

    /**
     * Test
     */
    public function delete($id)
    {
        $wg = Workinggroup::find($id);
        echo $wg->name;
        echo ' : supprimé de la base';
        $wg->delete($id);
    }

    /**
     * Test
     * création de workinggroup
     */
    public function createWorkinggroug1()
    {
        Workinggroup::create([
            'name' => 'Groupe récolte déchets'
        ]);
    }

    /**
     * Test
     */
    public function getPersons($id)
    {
        $persons = Workinggroup::find($id)->persons;
        foreach ($persons as $person) {
            echo $person->name;
            echo ' : ';
            echo $person->firstname;
            echo ' | ';
        }
        return $persons;
    }

    /**
     * Test
     *  Attention : il doit y avoir les colonnes created_at et updated_at
     * dans la table contributions.
     */
    public function saveWorkinggroug()
    {
        $wg = new Workinggroup(['name' => 'Groupe l\'environnement expliqué aux enfants']);
        $wg->save();
        /*         $pers1 = Person::find(1);
        $pers2 = Person::find(2);
        $wg->persons()->save($pers1);
        $wg->persons()->save($pers2); */
    }

    /**
     * Test
     */
    public function saveManyPerson()
    {
        $wg = Workinggroup::find(1);
        $pers1 = Person::find(1);
        $pers2 = Person::find(2);
        $wg->persons()->saveMany([
            new Person([
                'name' => 'Martin', 'firstname' => 'Florence', 'dateofbirthday' => '1999-01-01',
                'user' => '3000',
                'password' => '1234'
            ]),
        ]);
    }

    /*    public function getPersContrib2020() {
      $persons = Person::with(['contributions' => function ($query) {
         $query->where('year', '=', '20');
      }])->get();
     return $persons;
   } */

    /**
     * Test
     */
    public function saveProject1()
    {
        $pj = new Project([
            'label' => 'Plantations d\'arbres en forêt de Montmorency',
            'goal' => 'Régénérer la forêt',
            'begindate' => '2021-05-01',
            'enddate' => '2022-05-01',
            'image' => ''
        ]);
        $pj->save();
    }

    /**
     * Test
     */
    public function saveProject2()
    {
        $pj = new Project([
            'label' => 'Construction d\'une héolienne',
            'goal' => 'Sensibiliser les adolescents',
            'begindate' => '2021-05-01',
            'enddate' => '2022-05-01',
            'image' => ''
        ]);
        $pj->save();
    }

    /**
     * Test
     */
    public function saveManyProject()
    {
        $wg = Workinggroup::find(1);
        $pj1 = Project::find(1);
        $pj2 = Project::find(2);
        $wg->projects()->saveMany([
            $pj1, $pj2
        ]);
    }

    /**
     * Test
     */
    public function saveArticle1()
    {
        $pj = new Article([
            'title' => 'Les espèces en voie de disparition en France',
            'subtitle' => 'La France n\'est pas épargnée par la disparition d\'espèces',
            'content' => 'bla bla',
            'date' => '2021-03-04'
        ]);
        $pj->save();
    }

    /**
     * Test
     */
    public function saveArticle2()
    {
        $pj = new Article([
            'title' => 'Les énergies durables',
            'subtitle' => 'Comment remplacer les énergies fossiles',
            'content' => 'bla bla',
            'date' => '2021-03-04'
        ]);
        $pj->save();
    }

    /**
     * Test
     */
    public function saveManyArticle()
    {
        $wg = Workinggroup::find(1);
        $ar1 = Article::find(1);
        $ar2 = Article::find(2);
        $wg->articles()->saveMany([
            $ar1, $ar2
        ]);
    }
}
