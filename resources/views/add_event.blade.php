@extends('layouts.app')
@section('content')
<!-- <x-application-logo /> -->

<div class="container">
    <nav class="navbar navbar-expand-sm navbar-light bg-white fixed-top">
        <a class="navbar-brand" href="/">
            <img src="images/association/logo.jpg" alt="logo de l'association">
        </a>
        <ul class="navbar-nav">
            @isset($user)
            <li class="nav-item"><a class="nav-link" href="/admi">Administration</a></li>
            <li class="nav-item"><a class="nav-link" href="/edit_user">{{ $user->email }}</a></li>
            <li class="nav-item"><a class="nav-link" href="/logout">Déconnexion</a></li>
            @endisset
        </ul>
    </nav>
</div>

<div class="container" style="margin-top: 100px">
    <div class="row">
        <h1>Créer un évènement</h1>
    </div>
    <div class="row">
        <form action="/add_event" method="post">
            @csrf
            @if ($errors->any())
            <div class="alert alert-danger" role="alert">
                Merci de corriger les erreurs affichées.
            </div>
            @endif
            <div class="d-flex flex-row">
                <div class="form-inline">
                    <label for="label">Titre : &nbsp;</label>
                    <input type="text" class="form-control @error('label') is-invalid @enderror" id="label" name="label" placeholder="Titre" value="{{ old('label') }}">
                    @error('label')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-inline">
                    <label for="begindate">&nbsp;Date de début : &nbsp;</label>
                    <input type="date" class="form-control @error('begindate') is-invalid @enderror" id="begindate" name="begindate" value="{{ old('begindate') }}">
                    @error('begindate')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-inline">
                    <label for="enddate">&nbsp;Date de fin : &nbsp;</label>
                    <input type="date" class="form-control @error('enddate') is-invalid @enderror" id="enddate" name="enddate" value="{{ old('enddate') }}">
                    @error('enddate')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <label for="content">Description : &nbsp;</label>
                <textarea class="form-control @error('content') is-invalid @enderror" id="content" name="content" rows="10" cols="80">Description</textarea>
                @error('content')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
                <button type="submit" class="btn btn-success">Enregistrer</button>
            </form>
    </div>
</div>
@endsection