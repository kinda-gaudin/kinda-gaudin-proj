@extends('layouts.app')
@section('content')

<div class="container">
    <nav class="navbar navbar-expand-sm navbar-light bg-white fixed-top">
        <a class="navbar-brand" href="/">
            <img src="images/association/logo.jpg" alt="logo de l'association">
        </a>
        <ul class="navbar-nav">
            @auth
            <li class="nav-item"><a class="nav-link" href="#evenements">Evènements</a></li>
            <li class="nav-item"><a class="nav-link" href="#projets">Projets</a></li>
            <li class="nav-item"><a class="nav-link" href="#contacts">Contacts</a></li>
            <li class="nav-item"><a class="nav-link" href="/edit_user">{{ $user->email }}</a></li>
            <li class="nav-item"><a class="nav-link" href="/logout">Déconnexion</a></li>
            @endauth

            @guest
            <li class="nav-item"><a class="nav-link" href="/login">Connexion</a></li>
            @endguest
        </ul>
    </nav>
</div>

<!-- Section 2 : évènements -->
<div class="titre_evenements" id="evenements">

    <div class="image_evenements">
        <img src="images/evenements/evenements.png" alt="calendrier" width="60" height="60">
    </div>

    <h1>Evènements</h1>

</div>

<div class="container">
    <div class="row">
        <h2 style="margin-top: 40px">Rechercher un évènement</h2>
        <ul>
            <li>Tous les champs à vide => tous les évènements</li>
            <li>Uniquement "Titre" => tous les évènements comportant la chaîne de caractère dans le titre</li>
            <li>Uniquement "Période début" ET "Période fin" => tous les évènements dont la date de début est comprise dans la période</li>
            <li>Uniquement "Description" => tous les évènements comportant la chaîne de caractère dans la description</li>
            <li>Uniquement "Titre" ET "Période début" ET "Période fin" => tous les évènements comportant la chaîne de caractère dans le titre et dont la date de début est comprise dans la période</li>
        </ul>
    </div>
    <div class="row">
        <form action="/search_event" method="post">
            @csrf
            @if ($errors->any())
            <div class="alert alert-danger" role="alert">
                Merci de corriger les erreurs affichées.
            </div>
            @endif
            <div class="d-flex flex-row">
                <div class="form-inline">
                    <label for="label">Titre : &nbsp;</label>
                    <input type="text" class="form-control @error('label') is-invalid @enderror" id="label" name="label" placeholder="Titre" value="{{ old('label') }}">
                    @error('label')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-inline">
                    <label for="begindate">&nbsp;Période début : &nbsp;</label>
                    <input type="date" class="form-control @error('begindate') is-invalid @enderror" id="begindate" name="begindate" value="{{ old('begindate') }}">
                    @error('begindate')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-inline">
                    <label for="enddate">&nbsp;Période fin : &nbsp;</label>
                    <input type="date" class="form-control @error('enddate') is-invalid @enderror" id="enddate" name="enddate" value="{{ old('enddate') }}">
                    @error('enddate')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-inline">
                    <label for="content">&nbsp;Description : &nbsp;</label>
                    <input type="text" class="form-control @error('content') is-invalid @enderror" id="content" name="content" placeholder="Description" value="{{ old('content') }}">
                    @error('content')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <button type="submit" class="btn btn-success">Rechercher</button>
        </form>
    </div>
</div>

{{-- $searchEvents est défini et non null --}}
@isset($searchEvents)
<table class="table table-striped table-bordered table-hover table-responsive">
        <thead>
            <tr>
                <th>Titre</th>
                <th>Début</th>
                <th>Fin</th>
                <th>Description</th>
                <th>Numéro</th>
                <th>Type</th>
                <th>Nom</th>
                <th>Code Postal</th>
                <th>Ville</th>
                <th>Modifier</th>
            </tr>
        </thead>
        <tbody>

            @foreach ($searchEvents as $event)
            <tr>
                <td>{{ $event->label }}</td>
                <td>{{ date('d/m/Y', strtotime($event->begindate)) }}</td>
                <td>{{ date('d/m/Y', strtotime($event->enddate)) }}</td>
                <td>{{ $event->content }}</td>
                <td>{{ $event->number }}</td>
                <td>{{ $event->type }}</td>
                <td>{{ $event->name }}</td>
                <td>{{ $event->code }}</td>
                <td>{{ $event->town }}</td>
                <td><a href=" /edit_event/{{ $event->id }}">Modifier</a> <a href="/delete_event/{{ $event->id }}">Supprimer</a></td>
    </tr>
    @endforeach
    </tbody>
</table>
@endisset

<nav class="mb-3">
    <a href="/add_event" class="btn btn-success">Ajouter un évènement</a>
</nav>

<!-- Section 3 : projets -->
<div class="titre_evenements" id="projets">

    <div class="image_evenements">
        <img src="images/projets/projets-icone2.jpg" alt="projets" width="60" height="60">
    </div>
    <h1>Projets</h1>

</div>

<nav class="nav_projets">
    <ul>
        <li><a href="#projets">Projet 1</a><i class="fas fa-caret-down"></i></li>
        <li><a href="#projets">Projet 2</a><i class="fas fa-caret-down"></i></li>
        <li><a href="#projets">Projet 3</a><i class="fas fa-caret-down"></i></li>
        <li><a href="#projets">Projet 4</a><i class="fas fa-caret-down"></i></li>
    </ul>
</nav>

<div class="images_projets">
    <div class="ligne_images_projets">

        <div class="photo_du_projet">
            <img src="images/projets/5G-environnement-vignette.jpg" alt="Environnement et 5G">
            <div class="texte_item">
                <h1>Environnement et 5G</h1>
                <p>Les dangers de la 5G ?</p>
            </div>
            <i class="fas fa-eye"></i>
        </div>
        <div class="photo_du_projet">
            <img src="images/projets/stopper-les-plastiques.jpg" alt="Stopper les plastiques">
            <div class="texte_item">
                <h1>Stopper les plastiques</h1>
                <p>Stopper les plastiques</p>
            </div>
            <i class="fas fa-eye"></i>
        </div>
        <div class="photo_du_projet">
            <img src="images/projets/ecologie-a-l-ecole.jpg" alt="L'écologie et l'école">
            <div class="texte_item">
                <h1>L'écologie et l'école</h1>
                <p>L'écologie et l'école</p>
            </div>
            <i class="fas fa-eye"></i>
        </div>
        <div class="photo_du_projet">
            <img src="images/projets/ecologie-urbaine.jpg" alt="Ecologie urbaine">
            <div class="texte_item">
                <h1>Ecologie urbaine</h1>
                <p>Ecologie urbaine</p>
            </div>
            <i class="fas fa-eye"></i>
        </div>

    </div>

    <div class="ligne_images_projets">

        <div class="photo_du_projet">
            <img src="images/projets/ecologie-au-bureau.jpg" alt="Ecologie au bureau">
            <div class="texte_item">
                <h1>Ecologie au bureau</h1>
                <p>Ecologie au bureau</p>
            </div>
            <i class="fas fa-eye"></i>
        </div>
        <div class="photo_du_projet">
            <img src="images/projets/energie-durablejpg.jpg" alt="Les énergies durables">
            <div class="texte_item">
                <h1>Les énergies durables</h1>
                <p>Les énergies durables</p>
            </div>
            <i class="fas fa-eye"></i>
        </div>
        <div class="photo_du_projet">
            <img src="images/projets/les-metiers-de-l-ecologie.jpg" alt="Les métiers de l'écologie">
            <div class="texte_item">
                <h1>Les métiers de l'écologie</h1>
                <p>Les métiers de l'écologie</p>
            </div>
            <i class="fas fa-eye"></i>
        </div>
        <div class="photo_du_projet">
            <img src="images/projets/ramassage-des-dechets.jpg" alt="Ramassage des déchets">
            <div class="texte_item">
                <h1>Ramassage des déchets</h1>
                <p>Ramassage des déchets</p>
            </div>
            <i class="fas fa-eye"></i>
        </div>

    </div>
</div>

<!-- Section : contacts -->
<div class="titre_evenements" id="contacts">

    <div class="image_evenements">
        <img src="images/contacts/contact-300x300.png" alt="contacts" width="60" height="60">
    </div>
    <h1>Contacts</h1>

</div>

{{-- $contacts est défini et non null --}}
@isset($contacts)
<table class="table table-striped table-bordered table-hover table-responsive">
        <thead>
            <tr>
                <th>Nom</th>
                <th>Email</th>
                <th>Sujet</th>
                <th>Message</th>
            </tr>
        </thead>
        <tbody>

            @foreach ($contacts as $contact)
            <tr>
                <td>{{ $contact->name }}</td>
                <td>{{ $contact->email }}</td>
                <td>{{ $contact->subject }}</td>
                <td>{{ $contact->message }}</td>
    </tr>
    @endforeach
    </tbody>
</table>
@endisset

@endsection