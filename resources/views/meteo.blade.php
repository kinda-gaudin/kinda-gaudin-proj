@extends('layouts.app')
@section('content')
    <x-application-logo />
    <!-- /////////////////////// DEBUT METEO HTML POUR JAVASCRIPT ///////////////////////// -->
<div class="container">
        <!--Ville recherchée -->
        <div class="row">
            <div class="col-sm-4 bg-light">
                <!-- Formulaire de recherche pour une ville saisie -->
                <h4 class="pt-1"><strong>Météo pour la ville :</strong></h4>
                <!-- Zone de saisie d'une ville -->
                <div class="input-group mb-3">
                    <input type="text" class="form-control" id="ville_recherchee" aria-label="City Search" aria-describedby="bouton_rechercher">
                    <div class="input-group-append">
                        <button class=" btn btn-success text-light" type="button" id="bouton_rechercher">Rechercher</button>
                    </div>
                </div>
                <!-- Historique des villes saisies -->
                <button class=" btn btn-success" type="button" id="bouton_effacer">Effacer l'historique</button>
                <ul class="list-group">

                </ul>
            </div>   
            <div class="col-sm-8">
                <div class="row ml-2 border border-dark rounded">
                    <div class="col-sm-12" id="current-weather">
                        <h3 class="city-name mb-1 mt-2 bolder" id="ville_courante"></h3>
                        <p>Température courante : <span class="current" id="temperature_courante"></span></p>
                        <p>Hygrométrie courante : <span class="current" id="hygrometrie_courante"></span></p> 
                        <p>Vitesse du vent : <span class="current" id="vitesse_vent"></span></p>
                        <p>Indice UV : <span class="current bg-danger rounded py-2 px-2 text-white" id="indice_UV"></span></p> 
                    </div>
                </div>
                <!-- Prévisions à 5 jours-->
                <div class="col-sm-12" id ="future-weather">
                    <h3>Prévisions à 5 jours :</h3>
                    <div class="row text-light">
                        <div class="col-sm-2 btn-success  text-white ml-2 mb-3 p-2 mt-2 rounded" >
                            <p id="fDate0"></p>
                            <p id="fImg0"></p>
                            <p>Temp. : <span id="fTemp0"></span></p>
                            <p>Hum. : <span id="fhygrometrie_courante0"></span></p>
                        </div>
                        <div class="col-sm-2 btn-success  text-white ml-2 mb-3 p-2 mt-2 rounded" >
                            <p id="fDate1"></p>
                            <p id="fImg1"></p>
                            <p>Temp. : <span id="fTemp1"></span></p>
                            <p>Hum. : <span id="fhygrometrie_courante1"></span></p>
                        </div>
                        <div class="col-sm-2 btn-success  text-white ml-2 mb-3 p-2 mt-2 rounded">
                            <p id="fDate2"></p>
                            <p id="fImg2"></p>
                            <p>Temp. : <span id="fTemp2"></span></p>
                            <p>Hum. : <span id="fhygrometrie_courante2"></span></p>
                        </div>
                        <div class="col-sm-2 btn-success  text-white ml-2 mb-3 p-2 mt-2 rounded">
                            <p id="fDate3"></p>
                            <p id="fImg3"></p>
                            <p>Temp. : <span id="fTemp3"></span></p>
                            <p>Hum. : <span id="fhygrometrie_courante3"></span></p>
                        </div>
                        <div class="col-sm-2 btn-success  text-white ml-2 mb-3 p-2 mt-2 rounded" >
                            <p id="fDate4"></p>
                            <p id="fImg4"></p>
                            <p>Temp. : <span id="fTemp4"></span></p>
                            <p>Hum. : <span id="fhygrometrie_courante4"></span></p>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>

    <!--jQuery JS-->
    <script src="https://code.jquery.com/jquery.js"></script>

    <!-- ///////////////////////////////////////////////////////////////////////// -->
    <!-- ================ JAVASCRIPT APPELS APIS METEO 5 JOURS =================== -->
    <!-- ///////////////////////////////////////////////////////////////////////// -->
    <script src="{{ asset('js/main.js') }}"></script>

<!-- /////////////////////// Fin METEO HTML POUR JAVASCRIPT ///////////////////////// -->
    @endsection
