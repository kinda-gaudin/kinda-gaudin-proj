@extends('layouts.app')
@section('content')
<x-application-logo />
<div class="container">
    <div class="row">
        <h1>Modifier votre compte</h1>
    </div>
    <div class="row">
        <form action="/edit_user" method="post">
            @csrf
            @if ($errors->any())
            <div class="alert alert-danger" role="alert">
                Merci de corriger les erreurs affichées.
            </div>
            @endif
            <div class="d-flex flex-row">
                <div>
                    <input type="hidden" id="email_old" name="email_old" value="{{ $user->email }}">
                </div>
                <div class="form-inline">
                    <label for="name">Nom : &nbsp;</label>
                    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" placeholder="Nom" value="{{ $user->name }}">
                    @error('name')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-inline">
                    <label for="firstname">&nbsp;Prénom : &nbsp;</label>
                    <input type="text" class="form-control @error('firstname') is-invalid @enderror" id="firstname" name="firstname" placeholder="Prénom" value="{{ $user->firstname }}">
                    @error('firstname')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-inline">
                    <label for="dateofbirthday">&nbsp;Date de naiss. : &nbsp;</label>
                    <input type="date" class="form-control @error('dateofbirthday') is-invalid @enderror" id="dateofbirthday" name="dateofbirthday" value="{{ $user->dateofbirthday }}">
                    @error('dateofbirthday')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="form-inline">
                <label for="email">Mail de connexion : &nbsp;</label>
                <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" placeholder="Mail de connexion" value="{{ $user->email }}">
                    @error('email')
                        <div class=" invalid-feedback">{{ $message }}
            </div>
            @enderror
    </div>
    <div class="form-inline">
        <label for="password">Mot de passe : &nbsp;</label>
        <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password" placeholder="Mot de passe" value="{{ old('password') }}">
        @error('password')
        <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-inline">
        <label for="password_confirmation">Confirmation du mot de passe : &nbsp;</label>
        <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" id="password_confirmation" name="password_confirmation" placeholder="Mot de passe" value="{{ old('password_confirmation') }}">
        @error('password_confirmation')
        <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>
    <button type="submit" class="btn btn-success">Valider</button>
    </form>
</div>
</div>
@endsection