@extends('layouts.app')
@section('content')
    <x-application-logo />
    <div class="container">
        <div class="row">
            <h1>Se connecter</h1>
        </div>
        <div class="row">
            <form action="/login" method="post">
                @csrf
                @if ($errors->any())
                    <div class="alert alert-danger" role="alert">
                        Merci de corriger les erreurs affichées.
                    </div>
                @endif
                <div class="form-group">
                    <label for="email">Mail de connexion</label>
                    <input type="email" class="form-control @error('email') is-invalid @enderror" 
                        id="email" name="email" placeholder="Mail de connexion" value="{{ old('email') }}">
                    @error('email')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="password">Mot de passe</label>
                    <input type="password" class="form-control @error('password') is-invalid @enderror" 
                        id="password" name="password" placeholder="Mot de passe" value="{{ old('password') }}">
                    @error('password')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-success">Valider</button>
            </form>
        </div>
    </div>
    @endsection
