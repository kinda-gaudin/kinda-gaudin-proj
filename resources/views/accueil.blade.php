<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8" />
    <meta name="description" content="Association de Défense de l'Environnement et du Cadre de vie">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/styles.css" />
    <link rel="stylesheet" href="css/media_queries.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,900" rel="stylesheet">
    <title>Association de Défense de l'Environnement et du Cadre de vie</title>
</head>

<body>
    <div id="bloc_page">

        <header>
            <x-application-logo />
            <nav>
                <ul>
                    <li><a href="#evenements">Evènements</a></li>
                    <li><a href="#projets">Projets</a></li>
                    <li><a href="#contact">Contact</a></li>
                    <li><a href="/meteo">Météo</a></li>
                    <li><a href="/register">Adhésion</a></li>
                    @auth
                    <li><a href="/admi">Réservé Adhérents</a></li>
                    <li><a href="/edit_user">Compte {{ $user->email }}</a></li>
                    <li><a href="/logout">Déconnexion</a></li>
                    @endauth

                    @guest
                    <li><a href="/login">Connexion</a></li>
                    @endguest
                    <!-- 					@isset($user)
					<li>{{ $user->email }}</li>
					@endisset -->
                </ul>
            </nav>
        </header>

        <!-- Section 1 : écran d'accueil de bienvenue-->

        <section class="ecran_accueil" id="accueil">
            <img src="images/association/libellule.jpg" alt="libellule">
            <div class="texte_accueil">
                <h1 class="association">Association de Défense de l'Environnement et du Cadre de vie Bouffémont</h1>
                <!--                 <h2>Citation du jour</h2>
                <a href="#" class="bouton_vert_infos">Plus d'infos</a> -->
            </div>
            <div class="bordure_bas_slide"></div>
            <div class="bordure_visible_slide"></div>
        </section>

        <!-- Section 2 : évènements -->

        <section class="ecran_evenements">

            <div class="titre_evenements" id="evenements">
                <h1>Evènements</h1>
                <div class="deco_titre">
                    <div class="bordure_grise"></div>
                    <div class="rond_vert"><i class="fas fa-circle"></i></div>
                    <div class="bordure_grise"></div>
                </div>
                <p>Nous sommes une association de défense de l'environnement qui lutte pour préserver le Cadre de vie sur Bouffémont et participe aussi aux actions départementales.
                    <br />En liaison, soit avec les services de l'environnement préfectoraux soit avec les autres associations locales ou fédérales.
                </p>
            </div>

            <div class="contenu_evenements">

                <div class="image_evenements">
                    <img src="images/evenements/evenements.png" alt="calendrier">
                </div>

                <!-- Le commentaire qui suit n'apparaît pas dans le source HTML -->
                {{-- $events est défini et non null --}}
                @isset($events)
                <div class="presentation_des_evenements">
                    @foreach ($events as $event)
                    <div class="element_evenements">
                        <div class="icone-evenement">
                            <img src="images/evenements/icone-evenement.jpg" alt="calendrier" width="40" height="40">
                        </div>
                        <div class="texte_evenements">
                            <h2>Du {{ date('d/m/Y', strtotime($event->begindate)) }} au {{date('d/m/Y', strtotime($event->enddate)) }}</h2>
                            <h2>{{ $event->label }}</h2>
                            <p>{{ $event->content }}</p>
                        </div>
                    </div>
                    @endforeach
                </div>
                @endisset
            </div>
        </section>

        <!-- Section 3 : projets -->

        <section class="ecran_projets" id="projets">

            <div class="titre_projets">
                <h1>NOS PROJETS</h1>
                <div class="deco_titre">
                    <div class="bordure_grise"></div>
                    <div class="rond_vert"><i class="fas fa-circle"></i></div>
                    <div class="bordure_grise"></div>
                </div>
                <p>Participez à nos projets ou créez le vôtre</p>
            </div>

            <nav class="nav_projets">
                <ul>
                    <li><a href="#projets">Projet 1</a><i class="fas fa-caret-down"></i></li>
                    <li><a href="#projets">Projet 2</a><i class="fas fa-caret-down"></i></li>
                    <li><a href="#projets">Projet 3</a><i class="fas fa-caret-down"></i></li>
                    <li><a href="#projets">Projet 4</a><i class="fas fa-caret-down"></i></li>
                </ul>
            </nav>

            <div class="images_projets">
                <div class="ligne_images_projets">

                    <div class="photo_du_projet">
                        <img src="images/projets/5G-environnement-vignette.jpg" alt="Environnement et 5G">
                        <div class="texte_item">
                            <h1>Environnement et 5G</h1>
                            <p>Les dangers de la 5G ?</p>
                        </div>
                        <i class="fas fa-eye"></i>
                    </div>
                    <div class="photo_du_projet">
                        <img src="images/projets/stopper-les-plastiques.jpg" alt="Stopper les plastiques">
                        <div class="texte_item">
                            <h1>Stopper les plastiques</h1>
                            <p>Stopper les plastiques</p>
                        </div>
                        <i class="fas fa-eye"></i>
                    </div>
                    <div class="photo_du_projet">
                        <img src="images/projets/ecologie-a-l-ecole.jpg" alt="L'écologie et l'école">
                        <div class="texte_item">
                            <h1>L'écologie et l'école</h1>
                            <p>L'écologie et l'école</p>
                        </div>
                        <i class="fas fa-eye"></i>
                    </div>
                    <div class="photo_du_projet">
                        <img src="images/projets/ecologie-urbaine.jpg" alt="Ecologie urbaine">
                        <div class="texte_item">
                            <h1>Ecologie urbaine</h1>
                            <p>Ecologie urbaine</p>
                        </div>
                        <i class="fas fa-eye"></i>
                    </div>

                </div>

                <div class="ligne_images_projets">

                    <div class="photo_du_projet">
                        <img src="images/projets/ecologie-au-bureau.jpg" alt="Ecologie au bureau">
                        <div class="texte_item">
                            <h1>Ecologie au bureau</h1>
                            <p>Ecologie au bureau</p>
                        </div>
                        <i class="fas fa-eye"></i>
                    </div>
                    <div class="photo_du_projet">
                        <img src="images/projets/energie-durablejpg.jpg" alt="Les énergies durables">
                        <div class="texte_item">
                            <h1>Les énergies durables</h1>
                            <p>Les énergies durables</p>
                        </div>
                        <i class="fas fa-eye"></i>
                    </div>
                    <div class="photo_du_projet">
                        <img src="images/projets/les-metiers-de-l-ecologie.jpg" alt="Les métiers de l'écologie">
                        <div class="texte_item">
                            <h1>Les métiers de l'écologie</h1>
                            <p>Les métiers de l'écologie</p>
                        </div>
                        <i class="fas fa-eye"></i>
                    </div>
                    <div class="photo_du_projet">
                        <img src="images/projets/ramassage-des-dechets.jpg" alt="Ramassage des déchets">
                        <div class="texte_item">
                            <h1>Ramassage des déchets</h1>
                            <p>Ramassage des déchets</p>
                        </div>
                        <i class="fas fa-eye"></i>
                    </div>

                </div>
            </div>
        </section>

        <!-- Section 4 : carte avec un formulaire de contact-->

        <section class="ecran_contact" id="contact">

            <iframe class="carte" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2615.527607692993!2d2.3120291900246444!3d49.03859098181764!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e642c00fae5d43%3A0x246355cad0fc4c24!2s3%20All%C3%A9e%20des%20Gen%C3%AAts%2C%2095330%20Domont!5e0!3m2!1sfr!2sfr!4v1611504533572!5m2!1sfr!2sfr"></iframe>

            <div class="formulaire_contact">
                <form method="post" action="/contact">
                    @csrf
                    @if ($errors->any())
                    <div class="alert alert-danger" role="alert">
                        Merci de corriger les erreurs affichées.
                    </div>
                    @endif
                    <h1>Contact Info</h1>
                    <p>
                        Association de Défense de <br />
                        l'Environnement et du Cadre de vie<br />
                        3 allée des Genêts 95570 BOUFFEMONT <br />
                        Tel : 01.39.91.60.80 <br />
                    </p>

                    <p><input type="text" name="name" id="name" placeholder="Nom" size="30" maxlength="20" /><br />
                    @error('name')
                        <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                    </p>
                    <p><input type="email" name="email" id="email" placeholder="Email" size="30" maxlength="50" /><br /></p>
                    <p><input type="text" name="subject" id="subject" placeholder="Sujet" size="30" maxlength="50" /><br /></p>
                    <p><textarea name="message" id="message" placeholder="Message" rows="6" cols="31"></textarea><br /></p>
                    <p><input class="bouton_vert_envoyer" type="submit" value="Envoyer" /></p>

                </form>
            </div>
        </section>

    </div>
</body>

</html>