<!DOCTYPE html>
<html lang="fr">

<head>
    @stack('styles')
    @stack('scripts')
    <meta charset="utf-8" />
    <meta name="description" content="Association de Défense de l'Environnement et du Cadre de vie">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        nav li a:hover {
            color: #000000;
            border-bottom: 4px solid #32ca6c;
        }

        /* ------------------------------ Section 2 : évènements de l'association ------------------------------  */

        .titre_evenements {
            display: flex;
            flex-direction: row;
            justify-content: center;
            align-items: center;
            margin-top: 100px
        }
    </style>
    <title>App Name</title>
</head>

<body>
    <div class="container">
        @yield('content')
    </div>
</body>

</html>