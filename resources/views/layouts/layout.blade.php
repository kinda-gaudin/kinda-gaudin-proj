<!DOCTYPE html>
<html lang="fr">

<head>
	<meta charset="utf-8" />
	<meta name="description" content="Association de Défense de l'Environnement et du Cadre de vie">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="css/styles.css" />
	<link rel="stylesheet" href="css/media_queries.css" />
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,900" rel="stylesheet">
	<title>Association de Défense de l'Environnement et du Cadre de vie</title>
</head>

<body>
	<div id="bloc_page">

		<!-- L'en-tête du site -->

		<header>
			<x-application-logo />
			<nav>
				<ul>
					<li><a href="#evenements">Evènements</a></li>
					<li><a href="#projets">Projets</a></li>
					<li><a href="#partenaires">Partenaires</a></li>
					<li><a href="#articles">Articles</a></li>
					<li><a href="/register">Adhésion</a></li>
					<li><a href="#contact">Contact</a></li>
					<li><a href="/login">Connexion</a></li>
					@isset($user)
					<li>{{ $user->email }}</li>
					@endisset
				</ul>
			</nav>
		</header>

		{{ $slot }}
	
	</div>
</body>

</html>