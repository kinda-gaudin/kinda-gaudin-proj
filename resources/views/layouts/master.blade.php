<!DOCTYPE html>
<html lang="fr">
   <head>
        @stack('styles')
        @stack('scripts')
      <title>DemoLaravel - @yield('title')</title>
   </head>
   <body>
      @yield('content')
   </body>
</html>