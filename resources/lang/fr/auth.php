<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Les données de connexion ne correspondent pas à celles déjà enregistrées.',
    'password' => 'Le mot de passe fourni est incorrect.',
    'throttle' => 'Limite des tentatives de connexions autorisées atteinte. Veuillez SVP ré-essayer dans :seconds secondes.',

];
