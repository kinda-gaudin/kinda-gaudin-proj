<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | following language lines contain default error messages used by
    | validator class. Some of these rules have multiple versions such
    | as size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => ':attribute doit être accepté.',
    'active_url' => ':attribute n\'est pas une URL valide.',
    'after' => ':attribute doit être postérieure à la date :date.',
    'after_or_equal' => ':attribute doit être postérieure ou égale à la date :date.',
    'alpha' => ':attribute doit contenir seulement des lettres.',
    'alpha_dash' => ':attribute doit contenir seulement des lettres, des chiffres, des points ou des soulignés.',
    'alpha_num' => ':attribute  doit contenir seulement des lettres ou des chiffres.',
    'array' => ':attribute doit être un tableau.',
    'before' => ':attribute doit être antérieure à la date :date.',
    'before_or_equal' => ':attribute doit être antérieure ou égale à la date :date.',
    'between' => [
        'numeric' => ':attribute doit être compris entre :min and :max.',
        'file' => ':attribute doit être compris entre :min and :max kilobytes.',
        'string' => ':attribute doit être compris entre :min and :max characters.',
        'array' => ':attribute doit avoir entre :min and :max éléments.',
    ],
    'boolean' => ':attribute doit être vrai ou faux.',
    'confirmed' => ':attribute de confirmation ne correspond pas.',
    'date' => ':attribute n\'est pas une date valide.',
    'date_equals' => ':attribute doit être égal à :date.',
    'date_format' => ':attribute ne correspopnd pas au format :format.',
    'different' => ':attribute et :other doivent être différents.',
    'digits' => ':attribute doit être :digits chiffres.',
    'digits_between' => ':attribute doit être compris entre :min et :max chiffres.',
    'dimensions' => ':attribute a une dimension d\'image invalide.',
    'distinct' => ':attribute a une valuer dupliquée.',
    'email' => ':attribute doit être une adresse de courriel valide.',
    'ends_with' => ':attribute doit se terminer par un ou plusieurs :values.',
    'exists' => ':attribute sélectionné est invalide.',
    'file' => ':attribute doit être un fichier.',
    'filled' => ':attribute doit avoir une valeur.',
    'gt' => [
        'numeric' => ':attribute doit être plus grand que :value.',
        'file' => ':attribute doit être plus grand que :value kilobytes.',
        'string' => ':attribute doit être plus grand que :value caractères.',
        'array' => ':attribute doit avoir plus de :value valeurs.',
    ],
    'gte' => [
        'numeric' => ':attribute doit être plus grand ou égal que :value.',
        'file' => ':attribute doit être plus grand ou égal que :value kilobytes.',
        'string' => ':attribute doit être plus grand ou égal que :value caractères.',
        'array' => ':attribute doit avoir :value valeurs ou plus.',
    ],
    'image' => ':attribute doit être une image.',
    'in' => ':attribute sélectionné est invalide.',
    'in_array' => ':attribute n\'existe pas dans :other.',
    'integer' => ':attribute doit être un entier.',
    'ip' => ':attribute doit avoir une adresse IP valide.',
    'ipv4' => ':attribute doit avoir une adresse IPv4 valide.',
    'ipv6' => ':attribute doit avoir une adresse IPv6 valide.',
    'json' => ':attribute doit une chaîne JSON valide.',
    'lt' => [
        'numeric' => ':attribute doit être plus petit que :value.',
        'file' => ':attribute doit être plus petit que :value kilobytes.',
        'string' => ':attribute doit être plus petit que :value caractères.',
        'array' => ':attribute doit avoir moins de :value valeurs.',
    ],
    'lte' => [
        'numeric' => ':attribute doit être plus petit ou égal que :value.',
        'file' => ':attribute doit être plus petit ou égal que :value kilobytes.',
        'string' => ':attribute doit être plus petit ou égal que :value caractères.',
        'array' => ':attribute ne doit pas avoir plus de :value valeurs.',
    ],
    'max' => [
        'numeric' => ':attribute ne peut pas être plus grand que :max.',
        'file' => ':attribute ne peut pas être plus grand que :max kilobytes.',
        'string' => ':attribute ne peut pas être plus grand que :max caractères.',
        'array' => ':attribute ne peut avoir plus de :max valeurs.',
    ],
    'mimes' => ':attribute doit être un fichier de type :values.',
    'mimetypes' => ':attribute doit être un fichier de type :values.',
    'min' => [
        'numeric' => ':attribute doit être au moins :min.',
        'file' => ':attribute doit être au moins :min kilobytes.',
        'string' => ':attribute doit être au moins :min caractères.',
        'array' => ':attribute doit avoir au moins :min valeurs.',
    ],
    'multiple_of' => ':attribute doit être un multiple de :value',
    'not_in' => ':attribute sélectionné est invalide.',
    'not_regex' => ':attribute a un format invalide.',
    'numeric' => ':attribute doit être un nombre.',
    'password' => 'mot de passe incorrect.',
    'present' => ':attribute doit être renseigné.',
    'regex' => ':attribute a un format invalide.',
    'required' => ':attribute est obligatoire.',
    'required_if' => ':attribute est obligatoire quand :other a la valeur :value.',
    'required_unless' => ':attribute est obligatoire à moins que :other a la valeur :values.',
    'required_with' => ':attribute est obligatoire quand :values existe.',
    'required_with_all' => ':attribute est obligatoire quand :values existe.',
    'required_without' => ':attribute est obligatoire quand :values n\'existe pas.',
    'required_without_all' => ':attribute est obligatoire quand aucune des :values exite.',
    'same' => ':attribute and :other must match.',
    'size' => [
        'numeric' => ':attribute doit être de :size.',
        'file' => ':attribute doit être de :size kilobytes.',
        'string' => ':attribute doit être de :size caractères.',
        'array' => ':attribute doit contenir :size valeurs.',
    ],
    'starts_with' => ':attribute doit commencer par une des valeurs suivantes :values.',
    'string' => ':attribute doit être une chîne de caractères.',
    'timezone' => ':attribute doit être une zone valide.',
    'unique' => ':attribute a déjà été pris.',
    'uploaded' => ':attribute échec de l\'upload.',
    'url' => ':attribute a un format invalide.',
    'uuid' => ':attribute doit être un UUID valide.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'Message personnalisé',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
