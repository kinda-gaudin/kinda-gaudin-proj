// Déclaration des variables
let ville="";
let ville_recherchee = $("#ville_recherchee");
let bouton_rechercher = $("#bouton_rechercher");
let bouton_effacer = $("#bouton_effacer");
let ville_courante = $("#ville_courante");
let temperature_courante = $("#temperature_courante");
let hygrometrie_courante= $("#hygrometrie_courante");
let vitesse_vent=$("#vitesse_vent");
let indice_UV= $("#indice_UV");
let tableau_ville=[];

// L'utilisation des APIs du site openweathermap.org, nécessite de s'inscrire pour avoir une clé à transmettre par la suite.
let cleApiMeteo="a0aca8a89948154a4182dcecc780b513";

console.log("======================= TEST DEBUT METEO ==========================");

// Appel des APIs du site openweathermap.org en AJAX.
function donnerPrevisionMeteo(ville){
    // URL du site et appel AJAX.
    let requete= "https://api.openweathermap.org/data/2.5/weather?q=" + ville + "&APPID=" + cleApiMeteo;
    $.ajax({
        url:requete,
        method:"GET",
    }).then(function(reponse){
        // Analyse la réponse pour afficher la météo courante de la ville, la date du jour et les icônes correspondates à la météo. 
        console.log(reponse);
        // Récupération des propriétes des icônes.
        let iconeMeteo= reponse.weather[0].icon;
        let urlIcone="https://openweathermap.org/img/wn/"+iconeMeteo +"@2x.png";
        // Le format français de date est récupéré sur le site : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date
        let date=new Date(reponse.dt*1000).toLocaleDateString();
        // Analyse la réponse suivant la ville et concatène la date et l'icône.
        $(ville_courante).html(reponse.name +"("+date+")" + "<img src="+urlIcone+">");
        // Analyse la réponse pour afficher la température courante en degré Celsius (on soustrait le zéro absolu -273.15).
        let degreCelsius = reponse.main.temp - 273.15 ;
        // On prend 0 décimale.
        // &#8451; est le code HTML pour afficher ℃ 
        $(temperature_courante).html((degreCelsius).toFixed(0)+"&#8451");
        // Affiche l'hygrométrie (mesure du degré d'humidité présent dans l'air).
        $(hygrometrie_courante).html(reponse.main.humidity+"%");
        // Affiche la vitesse du vent en Km/H
        let vitesseVent=reponse.wind.speed;
        let vitesseVentKmh=(vitesseVent*2.237*1.609344).toFixed(0);
        $(vitesse_vent).html(vitesseVentKmh+" km/h");
        // Affiche l'indice UV.
        // L'indice UV est récupéré avec la fonction calculerIndiceUV dans laquelle on passe la longitude et la latitude en paramètres.
        calculerIndiceUV(reponse.coord.lon,reponse.coord.lat);
        prevision5Jours(reponse.id);
        if(reponse.cod==200){
            tableau_ville=JSON.parse(localStorage.getItem("nom_ville"));
            console.log(tableau_ville);
            if (tableau_ville==null){
                tableau_ville=[];
                tableau_ville.push(ville.toUpperCase()
                );
                localStorage.setItem("nom_ville",JSON.stringify(tableau_ville));
                ajouterDansHistorique(ville);
            }
            else {
                if(rechercher(ville)>0){
                    tableau_ville.push(ville.toUpperCase());
                    localStorage.setItem("nom_ville",JSON.stringify(tableau_ville));
                    ajouterDansHistorique(ville);
                }
            }
        }

    });
}

// L'évènement clic sur le bouton de recherche du formulaire, provoque l'affichage de la météo de la ville saisie.
function afficherMeteo(evenement){
    console.log("======= afficherMeteo(evenement) =======");
    evenement.preventDefault();
    if(ville_recherchee.val().trim()!==""){
        ville=ville_recherchee.val().trim();
        donnerPrevisionMeteo(ville);
    }
}

// Fonction qui retourne l'indice UV
function calculerIndiceUV(ln,lt){
    // Construit l'URL pour l'API renvoyant l'indice UV.
    let urlIndiceUV="https://api.openweathermap.org/data/2.5/uvi?appid="+ cleApiMeteo+"&lat="+lt+"&lon="+ln;
    $.ajax({
            url:urlIndiceUV,
            method:"GET"
            }).then(function(reponse){
                $(indice_UV).html(reponse.value);
            });
}
    
// Prévision à 5 jour pour la ville courante.
function prevision5Jours(villeId){
    let urlPrevision="https://api.openweathermap.org/data/2.5/forecast?id="+villeId+"&appid="+cleApiMeteo;
    $.ajax({
        url:urlPrevision,
        method:"GET"
    }).then(function(reponse){
        
        for (let i=0;i<5;i++){
            let date= new Date((reponse.list[((i+1)*8)-1].dt)*1000).toLocaleDateString();
            let codeIcone= reponse.list[((i+1)*8)-1].weather[0].icon;
            let urlIcone="https://openweathermap.org/img/wn/"+codeIcone+".png";
            let temperatureKelvin= reponse.list[((i+1)*8)-1].main.temp;
            let degreCelsius=(temperatureKelvin-273.15).toFixed(0);
            hygrometrie_courante= reponse.list[((i+1)*8)-1].main.humidity;
        
            $("#fDate"+i).html(date);
            $("#fImg"+i).html("<img src="+urlIcone+">");
            $("#fTemp"+i).html(degreCelsius+"&#8451");
            $("#fhygrometrie_courante"+i).html(hygrometrie_courante+"%");
        }
        
    });
}

// Recherche si une ville existe dans le tableau local des villes
function rechercher(uneVille){
    console.log("======= rechercher(uneVille) =======");
    for (let villeTemp of tableau_ville) {
        if(uneVille.toUpperCase()===villeTemp){
            return -1;
        }
    }
    return 1;
}

// Ajout de la ville saisie dans l'historique
function ajouterDansHistorique(uneVille){
    let listeVilles= $("<li>"+uneVille.toUpperCase()+"</li>");
    $(listeVilles).attr("class","list-group-item");
    $(listeVilles).attr("data-value",uneVille.toUpperCase());
    $(".list-group").append(listeVilles);
}

// Affiche les prévisions d'une ville sélectionnée dans l'historique.
function previsionVilleHistorique(event){
    let elementHistoriqueSelectionne=event.target;
    if (event.target.matches("li")){
        ville=elementHistoriqueSelectionne.textContent.trim();
        donnerPrevisionMeteo(ville);
    }

}

// Afficher la dernière ville rentrée dans l'historique
function chargerDerniereVille(){
    $("ul").empty();
    tableau_ville = JSON.parse(localStorage.getItem("nom_ville"));
    if(tableau_ville!==null){
        tableau_ville=JSON.parse(localStorage.getItem("nom_ville"));
        for(let i = 0 ; i < tableau_ville.length ; i++) {
            ajouterDansHistorique(tableau_ville[i]);
        }
        ville=tableau_ville[i-1];
        donnerPrevisionMeteo(ville);
    }

}
// Efface l'historique de recherche dans la page.
function effacerHistorique(event){
    event.preventDefault();
    tableau_ville=[];
    localStorage.removeItem("nom_ville");
    document.location.reload();

}
// Gestionnaires d'évènements.
$("#bouton_rechercher").on("click",afficherMeteo);
$(document).on("click",previsionVilleHistorique);
$(window).on("load",chargerDerniereVille);
$("#bouton_effacer").on("click",effacerHistorique);

console.log("======================= TEST FIN METEO ==========================");


